FROM openjdk:17-jdk-alpine
EXPOSE 587
RUN addgroup -S moviessharing && adduser -S nhankun164 -G moviessharing

# JRE fails to load fonts if there are no standard fonts in the image; DejaVu is a good choice,
# see https://github.com/docker-library/openjdk/issues/73#issuecomment-207816707
RUN apk add --update ttf-dejavu && rm -rf /var/cache/apk/*
RUN apk upgrade --update-cache --available && \
    apk add openssl && \
    rm -rf /var/cache/apk/*

ARG DEPENDENCY=build/dependency
COPY ${DEPENDENCY}/BOOT-INF/lib /app/lib
COPY ${DEPENDENCY}/META-INF /app/META-INF
COPY ${DEPENDENCY}/BOOT-INF/classes /app

USER nhankun164:moviessharing
WORKDIR /app
ENTRYPOINT ["java","-Xmx2048m","-cp",".:./lib/*","com.remitano.moviessharing.MoviesSharingApiApplication"]