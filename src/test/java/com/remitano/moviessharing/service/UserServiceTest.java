package com.remitano.moviessharing.service;

import com.remitano.moviessharing.entity.UserRequest;
import com.remitano.moviessharing.model.User;
import com.remitano.moviessharing.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class UserServiceTest {
    @InjectMocks
    private UserServiceImpl userService;

    @Mock
    private UserRepository userRepo;

    @Mock
    private PasswordEncoder passwordEncoder;

    @Test
    public void throwException_WhenRegisterExistedUser() throws Exception {

        when(userRepo.findByUsername("username")).thenReturn(Optional.of(new User(1L, "username", "123456", new SimpleDateFormat("yyyy-MM-dd").parse("2020-02-14"), true)));
        assertThrows(Exception.class, () -> userService.registerUser(new UserRequest("username", "123456")));
    }

    @Test
    public void shouldReturnUser_WhenRegisterNewUser() throws Exception {
        when(userRepo.findByUsername("username")).thenReturn(Optional.empty());
        when(passwordEncoder.encode("123456")).thenReturn("a1b2c3");
        User user = userService.registerUser(new UserRequest("username", "123456"));
        assertEquals("username", user.getUsername());
        assertEquals("a1b2c3", user.getPassword());
    }


}
