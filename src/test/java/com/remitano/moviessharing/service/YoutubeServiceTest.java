package com.remitano.moviessharing.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class YoutubeServiceTest {
    @InjectMocks
    private YoutubeService youtubeService;

    @Test
    public void shouldReturnId_whenExtractYoutubeId() {
        // given
        String url = "https://www.youtube.com/watch?v=64gdg";
        // when
        String id = youtubeService.extractYoutubeId(url);
        // then
        assertEquals("64gdg", id);
    }
}
