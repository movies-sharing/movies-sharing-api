package com.remitano.moviessharing.service;

import com.google.api.services.youtube.model.Video;
import com.google.api.services.youtube.model.VideoListResponse;
import com.google.api.services.youtube.model.VideoSnippet;
import com.remitano.moviessharing.model.Movie;
import com.remitano.moviessharing.repository.MovieRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class MovieServiceTest {
    @InjectMocks
    private MovieServiceImpl movieService;

    @Mock
    private MovieRepository movieRepository;

    @Mock
    private YoutubeService youtubeService;

    VideoListResponse videoListResponse = spy(VideoListResponse.class);



    @Test
    public void shouldReturnList_whenGetMovies() throws ParseException {
        List<Movie> movies = new ArrayList<>();
        movies.add(new Movie(1L, "Title 1", "description 1", "https://www.youtube.com/watch?v=64gdg", "64gdg", "user1", new SimpleDateFormat("yyyy-MM-dd").parse("2020-02-14")));
        movies.add(new Movie(2L, "Title 2", "description 2", "https://www.youtube.com/watch?v=64gdg", "64gdg", "user2", new SimpleDateFormat("yyyy-MM-dd").parse("2020-02-14")));
        when(movieRepository.findAllByOrderByCreatedAtDesc()).thenReturn(movies);
        List<Movie> result = movieService.getMovies();
        assertEquals(2, result.size());
        assertEquals("Title 1", result.get(0).getTitle());
        assertEquals("Title 2", result.get(1).getTitle());
    }

    @Test
    public void shouldReturnMovie_whenShareUrl() throws GeneralSecurityException, IOException, ParseException {
        Movie movie = new Movie(1L, "Title 1", "description 1", "https://www.youtube.com/watch?v=64gdg", "64gdg", "user1", new SimpleDateFormat("yyyy-MM-dd").parse("2020-02-14"));
//        when(youtubeService.extractYoutubeId("https://www.youtube.com/watch?v=64gdg")).thenReturn("64gdg");
        when(youtubeService.getVideo("https://www.youtube.com/watch?v=64gdg")).thenReturn(videoListResponse);
        ArrayList arrayListSpy = spy(ArrayList.class);
        arrayListSpy.add(spy(Video.class));
        when(videoListResponse.getItems()).thenReturn(arrayListSpy);
        when(videoListResponse.getItems().get(0).getSnippet()).thenReturn(spy(VideoSnippet.class));
        when(videoListResponse.getItems().get(0).getSnippet().getTitle()).thenReturn("Title 1");
        when(videoListResponse.getItems().get(0).getSnippet().getDescription()).thenReturn("description 1");
        when(videoListResponse.getItems().get(0).getId()).thenReturn("64gdg");
        when(movieRepository.save(any(Movie.class))).thenAnswer(i -> i.getArgument(0));
        Movie result = movieService.shareMovie("https://www.youtube.com/watch?v=64gdg", "user1");
        verify(youtubeService, times(1)).getVideo("https://www.youtube.com/watch?v=64gdg");
        verify(movieRepository, times(1)).save(any(Movie.class));
        assertEquals("Title 1", result.getTitle());
        assertEquals("description 1", result.getDescription());
        assertEquals("https://www.youtube.com/watch?v=64gdg", result.getYoutubeUrl());

        assertEquals("64gdg", result.getVideoId());
    }
}
