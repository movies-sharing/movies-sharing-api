package com.remitano.moviessharing;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import org.json.JSONException;
import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
class MoviesSharingApiApplicationTests {

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    void contextLoads() throws JSONException {
        String response = this.restTemplate.getForObject("/api/movies", String.class);
        System.out.println(response);
        DocumentContext context = JsonPath.parse(response);
        List<Integer> ids = context.read("$.data..id");
        assertThat(ids.size()).isGreaterThanOrEqualTo(0);
        JSONAssert.assertEquals("{code:200, message:'SUCCESS'}", response, false);
    }
}
