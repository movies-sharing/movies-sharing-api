package com.remitano.moviessharing.entity;
import lombok.Data;

@Data
public class Response<T> {
    private int code;
    private String message;
    private T data;

    public Response() {

    }

    public Response(int code, String message) {
        super();
        this.code = code;
        this.message = message;
    }

    public Response(int code, String message, T data) {
        super();
        this.code = code;
        this.message = message;
        this.data = data;
    }
}

