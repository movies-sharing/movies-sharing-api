package com.remitano.moviessharing.entity;

import lombok.Data;

@Data
public class MovieShareRequest {
    private String youtubeUrl;
}
