package com.remitano.moviessharing.config.security;

import com.remitano.moviessharing.model.User;
import com.remitano.moviessharing.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;


import static java.lang.String.format;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class CustomAuthService implements UserDetailsService {
    private final UserRepository userRepo;

    public CustomAuthService(UserRepository userRepo) {
        this.userRepo = userRepo;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws AuthenticationException {
        log.info("Use custom user details service");
        Optional<User> user = userRepo.findByUsername(username);
        if (user.isEmpty()) {
            log.error("User with username/phone number - {}, not found", username);
            throw new UsernameNotFoundException(format("User with username/phone number - %s, not found", username));
        }
        return user.get();
    }


}

