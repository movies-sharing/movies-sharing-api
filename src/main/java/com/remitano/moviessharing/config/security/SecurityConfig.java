package com.remitano.moviessharing.config.security;

import com.remitano.moviessharing.repository.UserRepository;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;

import java.util.Arrays;

@EnableGlobalMethodSecurity(
        securedEnabled = true,
        jsr250Enabled = true,
        prePostEnabled = true
)
@Configuration
public class SecurityConfig {
    private Logger logger = org.slf4j.LoggerFactory.getLogger(SecurityConfig.class);
    private UserRepository userRepo;
    private JwtTokenFilter jwtTokenFilter;

    @Autowired
    private CustomAuthService customAuthService;

    @Autowired
    public SecurityConfig(UserRepository userRepo, JwtTokenFilter jwtTokenFilter) {
        super();
        this.userRepo = userRepo;
        this.jwtTokenFilter = jwtTokenFilter;

        // Inherit security context in async function calls
        SecurityContextHolder.setStrategyName(SecurityContextHolder.MODE_INHERITABLETHREADLOCAL);
    }

    @Bean
    public AuthenticationProvider anipuppyAuthProvider() {
        return new CustomAuthProvider(passwordEncoder(), customAuthService);
    }

    // Set password encoding schema
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        // Enable CORS and disable CSRF
        http = http.cors().configurationSource(request -> {
            CorsConfiguration configuration = new CorsConfiguration();
            configuration.setAllowedOrigins(Arrays.asList("*"));
            configuration.setExposedHeaders(Arrays.asList("Authorization"));
            configuration.setAllowedMethods(Arrays.asList("GET", "POST", "PUT", "DELETE"));
            configuration.setAllowedHeaders(Arrays.asList("*"));
            return configuration;
        }).and().csrf().disable();

        // Set session management to stateless
        http = http
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and();
        // Set unauthorized requests exception handler
//        http = http
//                .exceptionHandling()
//                .authenticationEntryPoint(
//                        (request, response, ex) -> {
//                            logger.error("Unauthorized request - {}", ex.getMessage());
//                            response.sendError(HttpServletResponse.SC_UNAUTHORIZED, ex.getMessage());
//                        }
//                )
//                .and();

        // Set permissions on endpoints
//        http.authorizeHttpRequests()
//                .requestMatchers("/api/user/**").permitAll()
//                .anyRequest().authenticated();

        // Set custom authentication provider
        http.authenticationProvider(anipuppyAuthProvider());
        // Add JWT token filter
        http.addFilterBefore(jwtTokenFilter, UsernamePasswordAuthenticationFilter.class);
        return http.build();
    }

    // Used by spring security if CORS is enabled.
    // @Bean
    // public CorsFilter corsFilter() {
    //     UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
    //     CorsConfiguration config = new CorsConfiguration();
    //     config.setAllowCredentials(true);
    //     //  client domain names allowed to be accessed
    //     // (springboot2.4 the above joining this paragraph can solve the problem. allowedOrigins cannot contain the special value "*" problem )
    //     List<String> allowedOriginPatterns = new ArrayList<>();
    //     allowedOriginPatterns.add("*");
    //     config.setAllowedOriginPatterns(allowedOriginPatterns);
    //     config.setExposedHeaders(Arrays.asList("Authorization"));
    //     //config.addAllowedOrigin("*");
    //     config.addAllowedHeader("*");
    //     config.addAllowedMethod("*");
    //     source.registerCorsConfiguration("/**", config);
    //     return new CorsFilter(source);
    // }

    // @Bean
    // public CorsWebFilter corsWebFilter() {

    //     final CorsConfiguration corsConfig = new CorsConfiguration();
    //     corsConfig.setAllowCredentials(true);
    //     corsConfig.setAllowedOriginPatterns(Collections.singletonList("*"));
    //     corsConfig.setMaxAge(3600L);
    //     corsConfig.setExposedHeaders(Arrays.asList("Authorization"));
    //     corsConfig.setAllowedMethods(Arrays.asList("GET", "POST", "PUT", "DELETE"));
    //     corsConfig.addAllowedHeader("*");

    //     final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
    //     source.registerCorsConfiguration("/**", corsConfig);

    //     return new CorsWebFilter(source);
    // }

    // Expose authentication manager bean
    @Bean
    public AuthenticationManager authenticationManager(AuthenticationConfiguration authenticationConfiguration) throws Exception {
        return authenticationConfiguration.getAuthenticationManager();
    }

}
