package com.remitano.moviessharing.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.util.Date;

@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Entity
@Table(name = "movies")
@EntityListeners(AuditingEntityListener.class)
public class Movie {
    public Movie(Long id, String title, String description, String youtubeUrl, String videoId, String sharedBy, Date createdAt) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.youtubeUrl = youtubeUrl;
        this.videoId = videoId;
        this.sharedBy = sharedBy;
        this.createdAt = createdAt;
    }
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String title;
    private String description;
    private String youtubeUrl;
    private String videoId;
    private String sharedBy;
    @CreatedDate
    private Date createdAt;


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Movie movie)) return false;

        if (!getId().equals(movie.getId())) return false;
        if (!getTitle().equals(movie.getTitle())) return false;
        if (getDescription() != null ? !getDescription().equals(movie.getDescription()) : movie.getDescription() != null)
            return false;
        if (!getYoutubeUrl().equals(movie.getYoutubeUrl())) return false;
        if (!getSharedBy().equals(movie.getSharedBy())) return false;
        return getCreatedAt() != null ? getCreatedAt().equals(movie.getCreatedAt()) : movie.getCreatedAt() == null;
    }

    @Override
    public int hashCode() {
        int result = getId().hashCode();
        result = 31 * result + getTitle().hashCode();
        result = 31 * result + (getDescription() != null ? getDescription().hashCode() : 0);
        result = 31 * result + getYoutubeUrl().hashCode();
        result = 31 * result + getSharedBy().hashCode();
        result = 31 * result + (getCreatedAt() != null ? getCreatedAt().hashCode() : 0);
        return result;
    }
}
