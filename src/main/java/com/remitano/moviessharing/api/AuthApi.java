package com.remitano.moviessharing.api;

import com.remitano.moviessharing.config.security.UserJwtTokenUtil;
import com.remitano.moviessharing.entity.Response;
import com.remitano.moviessharing.entity.UserRequest;
import com.remitano.moviessharing.entity.UserView;
import com.remitano.moviessharing.model.User;
import com.remitano.moviessharing.service.UserService;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping(path = "api/user")
@Validated
@Slf4j
public class AuthApi {
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private UserJwtTokenUtil jwtTokenUtil;
    @Autowired
    private UserService userService;

    @PostMapping("/login")
    public ResponseEntity<Object> login(@RequestBody @Valid UserRequest request) throws Exception {
        try {
            Authentication authenticate = authenticationManager
                    .authenticate(new UsernamePasswordAuthenticationToken(request.getUsername(), request.getPassword()));

            User user = (User) authenticate.getPrincipal();
            return ResponseEntity.ok()
                    .header(HttpHeaders.AUTHORIZATION, jwtTokenUtil.generateAccessToken(user))
                    .body(new UserView(user.getUsername()));
        }
        catch (UsernameNotFoundException ex) {
            log.error("UsernameNotFoundException: {}", ex.getMessage());
            User user = userService.registerUser(request);
            return ResponseEntity.ok()
                    .header(HttpHeaders.AUTHORIZATION, jwtTokenUtil.generateAccessToken(user))
                    .body(new UserView(user.getUsername()));
        }
        catch (BadCredentialsException ex) {
            log.error("BadCredentialsException: {}", ex.getMessage());
//            User user = userService.registerUser(request);
//            return ResponseEntity.ok()
//                    .header(HttpHeaders.AUTHORIZATION, jwtTokenUtil.generateAccessToken(user))
//                    .body(user);
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }

        catch (AuthenticationException ex) {
            log.error("AuthenticationException: {}", ex.getMessage());
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
        catch (Exception ex) {
            log.error("Other Exception: {}", ex.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PostMapping("/token/verify")
    public ResponseEntity<Response<Map<String, Object>>> verifyToken(
            @RequestHeader(value = "Authorization") String authToken
    ) {
        String tokenParts = authToken.replace("Bearer", "");

        boolean isTokenValid = jwtTokenUtil.validate(tokenParts.trim());

        if (isTokenValid) {
            Map<String, Object> result = new HashMap<>();
            result.put("valid", true);
            return ResponseEntity.ok()
                    .body(new Response<>(HttpStatus.OK.value(), "SUCCESS", result));
        } else {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(new Response(HttpStatus.UNAUTHORIZED.value(), "JWT expired", "Token is expired"));
        }
    }

}
