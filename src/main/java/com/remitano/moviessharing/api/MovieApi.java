package com.remitano.moviessharing.api;

import com.remitano.moviessharing.config.security.CustomAuthService;
import com.remitano.moviessharing.config.security.UserJwtTokenUtil;
import com.remitano.moviessharing.entity.MovieShareRequest;
import com.remitano.moviessharing.entity.Response;
import com.remitano.moviessharing.exception.NotFoundException;
import com.remitano.moviessharing.model.Movie;
import com.remitano.moviessharing.service.MovieService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "api/movies")
@Slf4j
public class MovieApi {
    @Autowired
    private UserJwtTokenUtil jwtTokenUtil;
    @Autowired
    private CustomAuthService customAuthService;
    @Autowired
    private MovieService movieService;
    @GetMapping("")
    public ResponseEntity<Response<List<Movie>>> getMovies() {
        return ResponseEntity.ok().body(new Response<>(HttpStatus.OK.value(), "SUCCESS", movieService.getMovies()));
    }

    @PostMapping("/share")
    public ResponseEntity<Response<Movie>> shareMovie(
            @RequestHeader(value = "Authorization") String authToken,
            @RequestBody MovieShareRequest request
    ) {
        String tokenParts = authToken.replace("Bearer", "");
        try {
            if (jwtTokenUtil.validate(tokenParts)) {
                String username = jwtTokenUtil.getUsername(tokenParts);
                UserDetails userDetails = customAuthService.loadUserByUsername(username);
                Movie movie = movieService.shareMovie(request.getYoutubeUrl(), username);
                return ResponseEntity.ok().body(new Response<>(HttpStatus.OK.value(), "SUCCESS", movie));
            }
            else {
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
            }
        }
        catch (UsernameNotFoundException ex) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
        catch (NotFoundException ex) {
            log.error("NotFoundException url: {}", ex.getMessage());
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new Response<>(HttpStatus.NOT_FOUND.value(), "NOT_FOUND", null));
        }
        catch (Exception ex) {
            log.error("Exception: {}", ex.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}
