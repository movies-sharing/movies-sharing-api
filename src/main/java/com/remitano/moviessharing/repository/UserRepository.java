package com.remitano.moviessharing.repository;

import com.remitano.moviessharing.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository("userRepo")
public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByUsername(String username);
}