package com.remitano.moviessharing.repository;

import com.remitano.moviessharing.model.Movie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("movieRepo")
public interface MovieRepository extends JpaRepository<Movie, Long> {
    // find all with decreasing order by createdAt
    List<Movie> findAllByOrderByCreatedAtDesc();
}