package com.remitano.moviessharing.service;

import com.google.api.client.json.jackson2.JacksonFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;

import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.model.VideoListResponse;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Arrays;
import java.util.Collection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class YoutubeService {
    @Value("${youtube.apikey}")
    private String apiKey;

    private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();

    public YouTube getService() throws GeneralSecurityException, IOException {
        final NetHttpTransport httpTransport = GoogleNetHttpTransport.newTrustedTransport();
        return new YouTube.Builder(httpTransport, JSON_FACTORY, null)
                .setApplicationName("youtube_sharing")
                .build();
    }

    public VideoListResponse getVideo(String videoUrl) throws GeneralSecurityException, IOException, GoogleJsonResponseException {
        String videoId = extractYoutubeId(videoUrl);
        YouTube youtubeService = getService();
        YouTube.Videos.List request = youtubeService.videos().list("snippet");
        VideoListResponse response = request.setId(videoId)
                .setKey(apiKey)
                .execute();
        return response;
    }

    public String extractYoutubeId(String youtubetUrl) {
        String vId = null;
        Pattern pattern = Pattern.compile(
        "http(?:s)?:\\/\\/(?:m.)?(?:www\\.)?youtu(?:\\.be\\/|(?:be-nocookie|be)\\.com\\/(?:watch|[\\w]+\\?(?:feature=[\\w]+.[\\w]+\\&)?v=|v\\/|e\\/|embed\\/|live\\/|user\\/(?:[\\w#]+\\/)+))([^&#?\\n]+)",
                Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(youtubetUrl);
        if (matcher.matches()){
            vId = matcher.group(1);
        }
        return vId;
    }

}
