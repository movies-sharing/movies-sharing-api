package com.remitano.moviessharing.service;

import com.google.api.services.youtube.model.VideoListResponse;
import com.remitano.moviessharing.exception.NotFoundException;
import com.remitano.moviessharing.model.Movie;
import com.remitano.moviessharing.repository.MovieRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.List;

@Service
@Slf4j
public class MovieServiceImpl implements MovieService {
    @Autowired
    private MovieRepository movieRepo;
    @Autowired
    private YoutubeService youtubeService;

    @Override
    public List<Movie> getMovies() {
        List<Movie> movies = movieRepo.findAllByOrderByCreatedAtDesc();
        return movies;
    }

    @Override
    public Movie shareMovie(String youtubeUrl, String sharedBy) throws GeneralSecurityException, IOException, NotFoundException {
        VideoListResponse videoData = youtubeService.getVideo(youtubeUrl);
        if (videoData.getItems().size() == 0) {
            throw new NotFoundException("Video not found");
        }
        String title = videoData.getItems().get(0).getSnippet().getTitle();
        String description = videoData.getItems().get(0).getSnippet().getDescription();
        log.info("description: {}", description);
        log.info("description trunc: {}", description.substring(0, Math.min(description.length(), 200)));
        Movie movie = new Movie();
        movie.setSharedBy(sharedBy);
        movie.setYoutubeUrl(youtubeUrl);
        movie.setVideoId(videoData.getItems().get(0).getId());
        movie.setTitle(title);
        movie.setDescription(description.substring(0, Math.min(description.length(), 200)));
        movieRepo.save(movie);
        return movie;
    }
}
