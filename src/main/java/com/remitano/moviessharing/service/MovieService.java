package com.remitano.moviessharing.service;

import com.remitano.moviessharing.entity.UserRequest;
import com.remitano.moviessharing.exception.NotFoundException;
import com.remitano.moviessharing.model.Movie;
import com.remitano.moviessharing.model.User;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.List;

public interface MovieService {
    public abstract List<Movie> getMovies();
    public abstract Movie shareMovie(String youtubeUrl, String shareBy) throws GeneralSecurityException, IOException, NotFoundException;
}
