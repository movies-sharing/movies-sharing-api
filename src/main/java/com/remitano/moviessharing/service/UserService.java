package com.remitano.moviessharing.service;

import com.remitano.moviessharing.entity.UserRequest;
import com.remitano.moviessharing.model.User;

public interface UserService {
    public abstract User registerUser(UserRequest userRequest) throws Exception;
}
