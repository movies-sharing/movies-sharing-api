package com.remitano.moviessharing.service;

import com.remitano.moviessharing.entity.UserRequest;
import com.remitano.moviessharing.model.User;
import com.remitano.moviessharing.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;


@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepo;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public User registerUser(UserRequest userRequest) throws Exception {
        Optional<User> userOptional = userRepo.findByUsername(userRequest.getUsername());
        if (userOptional.isPresent()) {
            throw new Exception("User already exists");
        }
        User user = new User();
        user.setUsername(userRequest.getUsername());
        user.setPassword(passwordEncoder.encode(userRequest.getPassword()));
        userRepo.save(user);
        return user;
    }

}
