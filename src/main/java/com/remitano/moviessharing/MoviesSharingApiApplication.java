package com.remitano.moviessharing;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class MoviesSharingApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(MoviesSharingApiApplication.class, args);
    }

}
