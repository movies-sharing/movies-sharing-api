# Youtube Movie Sharing API
This is a simple API to share youtube movies. It is built using Spring Boot, Spring Data JPA and MySQL.

Getting Started
- [How to run this project?](#how-to-run-this-project)
- [Unit Tests](#unit-tests)

## How to run this project?
1. Clone this repository
```bash
>>> git clone https://gitlab.com/movies-sharing/movies-sharing-api.git
>>> cd movies-sharing-api
```
2. Create a database in MySQL
```bash
>>> mysql -u root -p
>>> CREATE DATABASE movies_db;
# create user and grant privileges
>>> CREATE USER 'mysqluser'@'localhost' IDENTIFIED BY '123456';
>>> GRANT ALL PRIVILEGES ON movies_db.* TO 'mysqluser'@'localhost';
``` 


3. Run the application
```bash
>>> ./gradlew bootRun
```

4. The application have 4 endpoints:
- POST /api/user/login 
  - Login an existed user or register a new user if not existed --> return a JWT token in header and username in response body
- POST /api/user/token/verify
    - Verify the token is whether expired or not
- GET /api/movies
    - Get all movies shared by all users
- POST /api/movies/share
    - Share a Youtube URL --> parse the video information through Youtube Data API and save to database


## Unit Tests
![test](results/testing.png)
